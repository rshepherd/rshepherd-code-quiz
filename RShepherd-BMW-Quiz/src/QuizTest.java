import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.*;
import static org.junit.Assert.*;

public class QuizTest
{
	// Class(es)
	StringFunctions func = new StringFunctions();
	
	// Captures the output for comparison for unit testing
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	String lineSep = System.getProperty("line.separator");
	
	@Before
	public void setUpStreams()
	{
		// Redirects output to our byte array so we can compare it
		System.setOut(new PrintStream(output));
	}
	
	@Test
	public void TestDifferentDigits() // getMinimumDigits(), numDigits()
	{
		String a = "1234567890",
			   b = "a1sdfghjkl",
			   c = "a1s2dfghjkl",
			   d = "a1s2d3fghjkl";
		String[] array = {a,b,c,d};
		String min = func.getMinimumDigits(array);
		assertEquals(min.equals(b), true);
	}
	
	@Test
	public void TestSameMinDigits() // getMinimumDigits(), numDigits()
	{
		String a = "a1s2d3",
			   b = "a1s2d3",
			   c = "12345",
			   d = "asdf12345";
		String[] array = {a,b,c,d};
		String min = func.getMinimumDigits(array);
		assertEquals(min.equals(a), true);
	}
	
	@Test
	public void TestNoDigits() // getMinimumDigits(), numDigits()
	{
		String a = "123",
			   b = "1234",
			   c = "12345",
			   d = "asdfghjkl";
		String[] array = {a,b,c,d};
		String min = func.getMinimumDigits(array);
		assertEquals(min.equals(d), true);
	}
	
	@Test
	public void TestMultiNoDigits() // getMinimumDigits(), numDigits()
	{
		String a = "1a",
			   b = "a1",
			   c = "asdf",
			   d = "asdfghjkl";
		String[] array = {a,b,c,d};
		String min = func.getMinimumDigits(array);
		assertEquals(min.equals(d), true);
	}
}
