NOTES:
    Using EC-Lemma to check the JUnit coverage, 100% of the code is covered
Makefile commands and usage:
    "make":         compiles the StringFunctions class
    "make test":    compiles the test class (QuizTest.java)
    "make runtest": runs the test
    "make all":     compiles all of the classes
    "make clean":   gets rid of the *.class files
Executing in Windows terminal:
    Compile Class: javac -g  StringFunctions.java
    Compile Test:  javac -cp .:junit.jar:hamcrest.jar QuizTest.java
    Running Test:  java  -cp .:junit.jar:hamcrest.jar org.junit.runner.JUnitCore QuizTest