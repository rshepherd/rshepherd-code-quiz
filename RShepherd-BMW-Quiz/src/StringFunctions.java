
public class StringFunctions
{
	//Constructor(s)
	public StringFunctions(){}
	
	public String getMinimumDigits(String[] arg0)
	{
		int    minLength = -1;
		String result    = null;
		for (String val : arg0)
		{
			int digits = numDigits(val);
			if ((digits < minLength) || (minLength == -1)
				|| ((digits == minLength) && (val.length() > result.length())))
			{
				minLength = digits;
				result    = val;
			}
		}
		return result;
	}
	
	private int numDigits(String arg0)
	{
		// removes non-digit characters from a string with a regular expression
		String justDigits = arg0.replaceAll("\\D+","");
		return justDigits.length();
	}
}
